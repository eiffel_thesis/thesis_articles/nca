#! /usr/bin/env gnuplot -c

# Produce svg output.
set terminal pdf;
# Since file are CSV the separtor is ';'.
set datafile separator ';';
# Set the key (i.e. plot title) outside below.
set key outside below;
# All our x axis are time.
set xlabel 'Time (second)';

bleu = "#2962ff"
ambre = "#ff9800"
vert = "#aeea00"

set output 'transactions_default.pdf';
set ylabel 'Transactions per second';
set yrange [0:850];

# It is possible to give column header directly.
plot 'transactions_A.csv' using 'time':'transactions_avg':'transactions_std' with errorbars title 'A' lt rgb bleu, \
	'transactions_B.csv' using 'time':'transactions_avg':'transactions_std' with errorbars title 'B' lt rgb ambre, \
	800 title 'Reference' lt rgb vert, \
	200 title '200' lt black;

set output 'memory_default.pdf';
set ylabel 'Memory (byte)';
set yrange [0:3 * 10 ** 9];

plot 'stats_A.csv' using 'iteration':'usage_avg':'usage_std' with errorbars title 'A' lt rgb bleu, \
	'stats_B.csv' using 'iteration':'usage_avg':'usage_std' with errorbars title 'B' lt rgb ambre, \
	2.8 * 10 ** 9 title 'Reference' lt rgb vert;

set output 'reads_default.pdf';
set ylabel 'Reads';
set yrange [0:2000];

plot 'stats_A.csv' using 'iteration':'reads_avg':'reads_std' with errorbars title 'A' lt rgb bleu, \
	'stats_B.csv' using 'iteration':'reads_avg':'reads_std' with errorbars title 'B' lt rgb ambre, \
	100 title 'Reference' lt rgb vert;