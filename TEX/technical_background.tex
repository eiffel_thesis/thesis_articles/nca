\section{Technical background}
\label{sec_technical}
A container is represented using the Linux \texttt{cgroup} mechanism \cite{hiroyu_cgroup_2008, rami_rosen_namespace_2016, linux_memory_nodate, zhenyun_zhuang_taming_2017}.
A \texttt{cgroup} is a set of processes whose collective usage of resources is limited.
In particular, the total amount of physical memory used across all the processes of a memory \texttt{cgroup} is capped by the \texttt{max} and \texttt{soft} limits described hereafter.
In the rest of this section, we study how Linux manages the memory of containers under \emph{memory pressure}, \textit{i.e.}  when free physical memory is scarce.

% Linux uses different mechanisms to manage memory \cite{the_kernel_development_community_concepts_nodate}.
% One of them is the page cache, when data from a file are read from disk they are stored in the page cache so subsequent accesses will occur in physical memory.
% Under memory pressure, Linux will, among other, free pages which belong to the page cache \cite{the_kernel_development_community_concepts_nodate, bovet_understanding_2006}.
% Indeed, free those data is not a problem because they can be read again from the disk.
% The pages which were freed can then be allocated to another process.
% There is one page cache for each memory \texttt{cgroup} on the system \cite{hiroyu_cgroup_2008, linux_memory_nodate}.

\subsection{No limits}
\label{subsec_technical_default}
When no limits are set and under memory pressure, memory will be reclaimed from all containers.
If containers have different memory needs, the kernel will allocate memory to satisfy them, even as their needs change during execution.
Indeed, if a container uses, at some moment of its execution, less memory than earlier the kernel will reallocate the unused memory to another container.
This constitutes memory consolidation; however there will not be memory performance isolation.
Indeed, if a container uses less its memory it will lose it, because it will be reclaimed.
In summary, when no limits are set, memory consolidation occurs at the expense of memory performance isolation.
% In cloud, memory performance isolation is important to ensure that clients services run smoothly.

\subsection{The \texttt{max} and \texttt{soft} limit mechanism}
\label{subsec_technical_max_soft}
Linux has two mechanisms to control memory reclamation of containers.
The first is the \texttt{max} limit; a container will not be allocated a memory footprint larger than its \texttt{max} limit, even if there exists unused memory elsewhere.
If a process needs physical memory, and its container has already reached its \texttt{max} limit, the kernel may reallocate memory from another process of the same container, but not from another container.
This mechanism avoids situations where some containers would starve the others.

The \texttt{soft} limit is similar to the \texttt{max} limit, except that it is active only when there is memory pressure and it is only best-effort.
The intent is that the system administrator will set a \texttt{soft} limit approximating the container's Working Set (WS) size \cite{denning_working_1967}.
Unfortunately, it is hard to estimate WS size \cite{gregg_working_2018, nitu_working_2018}.

In other words, the size of a container is limited to its \texttt{soft} limit when there is memory pressure, otherwise, to its \texttt{max} limit.
However, these limits are not directly related to the containers' current memory needs; they may be higher (impeding memory consolidation) or lower (impeding memory performance isolation).