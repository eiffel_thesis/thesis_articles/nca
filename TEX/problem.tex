\section{Memory consolidation \textit{VS.} memory performance isolation}
\label{sec_problem}
\subsection{Reference experiment}
\label{subsec_problem_reference}
In order to demonstrate the issues experimentally, we first run a reference experiment.
This experiment consists of executing a benchmark, detailed hereafter, to obtain reference number for throughput and memory footprint .
The throughput was obtained from the benchmark output while memory and inputs were collected by \texttt{docker}.
We measure the max throughput, under high offered load, to be 800 transactions per second.
The reference memory footprint was measured to be \SI{2.8}{\giga\byte}.
These numbers are pictured in Figure \ref{fig_default} to Figure \ref{fig_soft} as horizontal green lines.
In the scenario that we will describe in next section, the high offered load corresponds to the generation of 800 transactions per second.
We defined low offered load as what is required to reach 200 transactions per second.
This number is showed as horizontal black line in Figure \ref{fig_transactions} to Figure \ref{fig_transactions_soft}.

\subsection{Experimental scenario}
\label{subsec_problem_scenario}
We run an experiment whose goal is to show that without limits there is no memory performance isolation, and that the \texttt{max} and \texttt{soft} limits impede memory consolidation.
Our experimental scenario has two containers, A and B, which run an OLTP workload and experience changes in activity.
We expect that, when both containers have high offered load, memory performance isolation should occur.
When one container has high offered load and the other has low offered load or is stopped, memory consolidation should allow it to reach its reference performance.
Specifically, the experiment has 6 steps:
\begin{enumerate}[$\varphi_1$]
	\item\label{step_1} A and B both have high offered loads.
	Containers will bring database records in memory but physical memory available is inferior to databases' sizes.
	So, there will be memory pressure.
	Containers will then have to access the disk so their throughputs will be low.
	\item\label{step_2} The high offered load continues for A, and it decreases for B.
	If memory consolidation is effective A should be able to take memory from B, and increase its throughput to the reference value.
	\item\label{step_3} B has high offered load.
	This step is similar to step 1.
	So, containers will have a low throughput.
	\item\label{step_4} Both containers have low offered loads.
	We expect that they will be able to answer to all the transactions.
	\item\label{step_5} B has high offered load, not A.
	This step is symmetric to step 2.
	\item\label{step_6} A is completely stopped.
	B still has high offered load.
	We expect B to perform the same as the reference.
\end{enumerate}
Each step of the scenario lasts for 180 seconds.

\subsection{Experimental environment}
\label{subsec_problem_environment}
Our experimental machine is a workstation with an Intel\textregistered  Xeon\textregistered  E5-1603 clocked at \SI{2.8}{\giga\hertz}, \SI{8}{\giga\byte} of DDR3 and a SSD.
To create memory pressure, we run our experiments in a virtual machine (VM) restricted to 4 CPU cores and \SI{3}{\giga\byte} of memory.

We use \texttt{qemu 2.8.1} as VM hypervisor, \texttt{docker 18.09.6}  and its \texttt{python} library \texttt{docker-py 3.7.2} to manage containers.
Our containers execute the benchmark \texttt{sysbench oltp}.
We modified this benchmark to dynamically change the rate of transactions generated.
The benchmark makes request to a database managed by \texttt{mysql 5.7}.
We run our experiments on Linux 4.19.

Our two containers (A and B) run with 2 cores each.
They each read a database of \SI{4}{\giga\byte}.
Since our VM has only \SI{3}{\giga\byte} of memory, the two databases will not fit in memory and some accesses to those databases will not be cached by Linux page cache resulting in physical I/O, which heavily decrease the throughput.

Then, we run the scenario described in Section \ref{subsec_problem_scenario} 10 times and compute the mean and standard deviation every second.
Each point in curves depicted in Figure \ref{fig_default}, Figure \ref{fig_max} and Figure \ref{fig_soft} is the mean and its associated standard deviation for this second across the 10 runs.
% First, we measure the performance of A and B in the absence of the other, with no limit set; this give us reference performance (800 transactions per second during high activity period and 200 during low activity period).
% We also collect information about memory and reads to disk thanks to \texttt{docker}.
% All those numbers are pictured in Figure \ref{fig_default}, Figure \ref{fig_max}, Figure \ref{fig_soft} and Figure \ref{fig_mechanism} with horizontal green and black lines.
% For example, the reference memory footprint equals \SI{2.8}{\giga\byte} and is depicted as the green line in Figure \ref{fig_memory}, Figure \ref{fig_memory_max}, Figure \ref{fig_memory_soft} and Figure \ref{fig_memory_mechanism}.

\input{TEX/figure.tex}

\subsection{Experiment with no limits set}
\label{subsec_problem_without}
We run the experiment described in Section \ref{subsec_problem_scenario} without setting any limits.
By doing so, we wish to demonstrate that all containers are reclaimed equally, and there is no memory performance isolation.
Our results are depicted in Figure \ref{fig_default}.

Figure \ref{fig_transactions} shows the containers' throughputs over time and Figure \ref{fig_memory} depicts their memory footprints.
In Figure \ref{fig_transactions}, the throughput of a container that has high offered load increases at the expense of one with low offered load ($\varphi_2$ and $\varphi_5$).
Figure \ref{fig_memory} shows that the memory footprint of the container with high offered load increases, while the container with low offered load shrinks.
Memory consolidation is taking place but imperfectly, since an active container never reaches the reference level of performance.
When A stops, B's footprint grows, so its performance increases, reaching the reference level ($\varphi_6$).
When both containers have high offered loads they have the same throughput because they have the same memory footprint, due to the lack of memory performance isolation ($\varphi_1$ and $\varphi_3$).

\subsection{Experiment with \texttt{max} limit}
\label{subsec_problem_max}
We demonstrate that \texttt{max} limit impedes memory consolidation, by running the same experiment with \texttt{max} limits set to \SI{1.8}{\giga\byte} for A and \SI{1}{\giga\byte} for B.
Those values were chosen such that their total equals the reference memory footprint.

In Figure \ref{fig_transactions_max}, A has better performance than B.
This can be explained by looking at Figure \ref{fig_memory_max}.
Because the memory footprints reach the \texttt{max} limits, A has more memory than B; therefore it will be able to answer more requests ($\varphi_1$ and $\varphi_3$).
Figure \ref{fig_memory_max} shows that there is no memory consolidation, because even when B has low offered load, A's memory footprint does not increase, and it never reaches the reference level ($\varphi_2$).
Conversely, when A has low offered load and B has high offered load, B's memory does not grow, because of its \texttt{max} limit ($\varphi_5$).
Therefore, B reaches only 500 to 550 transactions per second.
Worst, when A stops, B is not able to expand its memory footprint and experiences little increase in performance ($\varphi_6$).

To conclude, with the \texttt{max} limit there is memory performance isolation, since A acquires more memory than B, but memory consolidation is impeded.

\subsection{Experiment with \texttt{soft} limit}
\label{subsec_problem_soft}
Under memory pressure, the \texttt{soft} limit has a similar effect to the \texttt{max} limit, also impeding memory consolidation.
We show this behavior by executing the experiment using \texttt{soft} limits.
Figure \ref{fig_soft} shows that the results are nearly the same as above.
This is expected because during our experiment there is memory pressure, and containers will tend to remain under the \texttt{soft} limits, which equal the previous \texttt{max} limits.
Note however how, in $\varphi_6$, B's footprint increases; because A was stopped, relaxing memory pressure, the \texttt{soft} limit mechanism is not activated anymore, allowing B to increase its memory allocation.

To summarize, the \texttt{soft} limit mechanism supports memory performance isolation (B has less memory than A) under memory pressure.
This limits memory consolidation but when memory pressure disappears, memory consolidation becomes effective again.

\subsection{Summary}
\label{subsec_problem_summary}
Our study shows that the existing mechanisms are not effective at memory consolidation and memory performance isolation.
Without limits, there is weak memory consolidation but no memory performance isolation.
The \texttt{max} limit supports memory performance isolation, but impedes memory consolidation.
The \texttt{soft} limit is analogous to the \texttt{max} limit under memory pressure, but when memory is plentiful it does enable memory consolidation.
We summarize these results in Table \ref{table_summary}.

\begin{table}
	\centering

	\begin{tabular}{|l|c|c|}
		\hline
		Mechanism used & Memory consolidation & Memory performance isolation\\
		\hline
		no limits & weak & no\\
		\hline
		\texttt{max} limit & no & yes\\
		\hline
		\texttt{soft} limit & no & yes\\
		\hline
	\end{tabular}

	\caption{Summary of memory consolidation and memory performance isolation of existing mechanisms}
	\label{table_summary}
\end{table}