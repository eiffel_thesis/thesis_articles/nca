\definecolor{violet_profond}{RGB}{103, 58, 183}
\definecolor{sarcelle}{RGB}{0, 150, 136}


\section{Evaluation}
\label{sec_evaluation}
We expect that NRHS will consolidate memory while ensuring memory performance isolation.
We will show this by running the experiment with the same limits as in Section \ref{subsec_problem_soft}, under NRHS.
We plot the results in Figure \ref{fig_mechanism}.

Figure \ref{fig_transactions_mechanism} shows that, when B has high offered load and A low offered load, NRHS permits B to reach the reference throughput ($\varphi_5$).
So, NRHS adds consolidation to the \texttt{soft} limit mechanism.
Generally, when one container receives a high offered load, it can reach the reference throughput ($\varphi_2$ and $\varphi_5$).
Moreover, when both containers are active, their throughputs are better than the previous results ($\varphi_1$ and $\varphi_3$).
Globally, A has better performance than B.
We compile integer part of median value for all steps in Table \ref{table_sumup}.

\begin{table*}
	\centering

	\begin{tabular}{l|c|c|c|c|c|c|c|c|c|c|c}
		\hline
		\diagbox{Transactions}{Step} & \multicolumn{2}{c|}{1} & \multicolumn{2}{c|}{2} & \multicolumn{2}{c|}{3} & \multicolumn{2}{c|}{4} & \multicolumn{2}{c|}{5} & 6\\
		\hline
		Container & A & B & A & B & A & B & A & B & A & B & B\\
		\hline
		Throughput & 800 & 800 & 800 & 200 & 800 & 800 & 200 & 200 & 200 & 800 & 800\\
		\hline
		No limits set & 541 & 556 & 684 & 189 & 555 & 554 & 193 & 193 & 190 & 670 & 776\\
		\texttt{Max} limit & 603 & 497 & 718 & 190 & 626 & 497 & 193 & 193 & 191 & 539 & 586\\
		\texttt{Soft} limit & 628 & 506 & 727 & 192 & 642 & 514 & 194 & 194 & 192 & 543 & 772\\
		NRHS & 697 & 561 & 797 & 193 & 715 & 573 & 194 & 194 & 192 & 773 & 763\\
	\end{tabular}

	\caption{Throughput in each step of each experiment (in transactions per second)}
	\label{table_sumup}
\end{table*}

Consideration of Figure \ref{fig_memory_mechanism} shows that when containers both receive high offered loads, their memory footprints follow their \texttt{soft} limits ($\varphi_1$ and $\varphi_3$).
A has then a bigger footprint than B, so NRHS ensures memory performance isolation.
When A alone faces a high offered load, its memory footprint decreases ($\varphi_2$).
Conversely, when B has to handle a high offered load, its memory footprint increases ($\varphi_5$).
When A stops completely, B's footprint grows, because there is no more memory pressure ($\varphi_6$).

Inputs from disk are plotted in Figure \ref{fig_disk_mechanism}.
When both containers face a high offered load ($\varphi_1$ and $\varphi_3$), their inputs follow the same pattern as in Figure \ref{fig_disk_soft}.
When A receives a high offered load and B low offered load, A does more disk inputs ($\varphi_2$).
In the opposite case, B reduces its disk inputs ($\varphi_5$) and when A stops, B's inputs drop to the reference level ($\varphi_6$).

\begin{figure}
	\begin{tikzpicture}
		\begin{axis}[xlabel = Time (in second), ylabel = Number of call, legend style = {at = {(0.5,-0.2)}, anchor = north, legend columns = -1}]
			\addplot[violet_profond] table [x = time, y = calls, col sep = semicolon]{CSV/soft/calls.csv};
			\addlegendentry{\texttt{Soft} limit}
			\addplot[sarcelle] table [x = time, y = calls, col sep = semicolon]{CSV/mechanism/calls.csv};
			\addlegendentry{NRHS}
		\end{axis}
	\end{tikzpicture}

	\caption{Number of call to \texttt{mem\_cgroup\_soft\_limit\_reclaim} over time during the scenario}
	\label{fig_calls}
\end{figure}

\begin{figure}
	\begin{tikzpicture}
		\pgfplotsset{set layers}
		\begin{axis}[ymin = 0, xmin = 0, xmax = 3, xticklabels = {Calls}, axis y line* = left, xmin = 0, xmax = 3, xtick = data, ylabel = Number of calls, ybar = 0, legend style = {at = {(0.5,-0.1)}, anchor = north, legend columns = -1, fill = none}]
			\addplot[color = violet_profond, fill, error bars/.cd, y dir = both, y explicit, error bar style = {black}] coordinates{
				(1, 17231.8) +- (0, 734.786745)
			};
			\addlegendentry{\texttt{Soft} limit}
			\addplot[color = sarcelle, fill, error bars/.cd, y dir = both, y explicit, error bar style = {black}] coordinates{
				(1, 11095.3) +- (0, 1022.760583)
			};
			\addlegendentry{NRHS}
		\end{axis}

		\begin{axis}[ymin = 0, xmin = 0, xmax = 3, xticklabels = {Time}, axis y line* = right, ylabel near ticks, yticklabel pos = right, xtick = data, ybar = 0, legend = none, ylabel = Time (in seconds)]
			\addplot[color = violet_profond, fill, error bars/.cd, y dir = both, y explicit, error bar style = {black}] coordinates{
				(2, 2.003918) +- (0, 0.039113)
			};
			\addplot[color = sarcelle, fill, error bars/.cd, y dir = both, y explicit, error bar style = {black}] coordinates{
				(2, 3.458549) +- (0, 0.122203)
			};
		\end{axis}
	\end{tikzpicture}

	\caption{Total time passed in \texttt{mem\_cgroup\_soft\_limit\_reclaim} and total calls to \texttt{mem\_cgroup\_soft\_limit\_reclaim} during the scenario}
	\label{fig_total_calls}
\end{figure}

\begin{figure}
	\begin{tikzpicture}
		\begin{axis}[xticklabels = \empty, ymin = 0, ybar, bar width = 28pt, ylabel = Time (in seconds), legend style = {at = {(0.5,-0.1)}, anchor = north, legend columns = -1}]
			\addplot[violet_profond, fill] table [x expr = \coordindex, y = system_time, col sep = semicolon]{CSV/soft/time.csv};
			\addlegendentry{\texttt{Soft} limit}
			\addplot[sarcelle, fill] table [x expr = \coordindex, y = system_time, col sep = semicolon]{CSV/mechanism/time.csv};
			\addlegendentry{NRHS}
		\end{axis}
	\end{tikzpicture}

	\caption{Total time passed in kernel}
	\label{fig_time}
\end{figure}

Note that, the performance of A is better, although its memory footprint is the same as in Figure \ref{fig_memory_soft}.
It also does more disk inputs compared to Figure \ref{fig_disk_soft}.
So, the action of NRHS on memory for A is insufficient to explain its performance increase.
Therefore, we also monitor the calls to \texttt{mem\_cgroup\_soft\_limit\_reclaim} with \texttt{trace-cmd} \cite{rostedt_trace_cmd_2010}.
Figure \ref{fig_calls} shows the number of calls for the \texttt{soft} limit and NRHS.
The first sixty seconds of the plot can be ignored (pre-experimental warm up).
During step $\varphi_6$, \texttt{mem\_cgroup\_soft\_limit\_reclaim} is not called at all because there is no memory pressure.
When both containers have high offered loads, the number of calls to the function is approximately the same, with either NRHS or the \texttt{soft} limit.
When only one container has high offered load, or when both have low offered load, there are fewer calls to the function with NRHS.

We also monitor the time spent in \texttt{mem\_cgroup\_soft\_limit\_reclaim}.
We plot the mean number of calls and the mean time duration for the whole experiment in Figure \ref{fig_total_calls}.
The function \texttt{mem\_cgroup\_soft\_limit\_reclaim} is called less often under NRHS ($\approx 11000$ calls) than with  \texttt{soft} limit ($\approx 17000$ calls), but its duration is larger (\SI{\approx 3.5}{\second}) compared to \texttt{soft} limit (\SI{\approx 2}{\second}).

We also monitor the total time spent in the kernel of the virtual machine from its startup until the end of the experiment by reading \texttt{/proc/stat}.
These numbers are depicted in Figure \ref{fig_time}.
NRHS reduces the time spent in kernel from \SI{\approx 8600}{\second} to \SI{\approx 5500}{\second}.
This explains the increase in performance under NRHS.
Firstly, the CPU will be used to effectively handle requests instead of running the \texttt{soft} limit mechanism code.
% TODO Vérifier cette phrase.
More importantly, since NRHS takes place before the \texttt{soft} limit and as it reclaims less memory than the former it disturbs the page cache less.
So, with NRHS the containers will not have to reload pages which were in their active lists so their inputs from disk will be less blocking.