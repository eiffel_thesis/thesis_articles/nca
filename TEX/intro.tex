\section{Introduction}
\label{sec_intro}
This paper is about showing the problem of memory consolidation for containers in Linux.

Logical servers are \emph{consolidated} by running several of them on a single physical machine, thereby saving money.
The logical servers should remain mutually isolated, \emph{i.e.} execution of one logical server should not impact any other.

One popular consolidation mechanism, in Linux is called ``containers'' \cite{amazon_amazon_nodate, microsoft_microsoft_nodate, alibaba_alibaba_nodate, ovh_ovh_nodate}.
A container is a group of processes isolated from the others.
Containers are advantageous for security (\textit{e.g.} a file that belongs to a container can not be read from another), deployment (\textit{e.g.} running a container is as simple as running a shell command) and resource control (\textit{e.g.} a container can be restricted to, say, a specific CPU core) \cite{docker_inc_what_nodate}.

This article focuses specifically on container \emph{memory consolidation}, which consists of multiplexing the physical memory of the machine across the virtual memory demands of all containers.
Memory consolidation can be effective if, at every point in time, the total memory demands of all containers is less than the available physical memory, which is the common case, because data center utilization usually remains well under 100\% \cite{meisner_powernap_2009, barroso_datacenter_2013}.
% For example, it is advantageous to consolidate a continuous integration service, which is active during office hours while developers are working \cite{weins_where_2017}.
% A recent study based on real traces also showed that some services can be characterized by long-lived but mostly-idle; for instance the website of ski resort \cite{BaToTcHaLeZw2019_1}.
% In practice, containers and virtual machines are often oversized by cloud clients \cite{weins_where_2017, de_cicco_resource_2013}.

The classical virtual memory mechanism ensures security isolation.
However, it is insufficient for \emph{memory performance isolation} (ensuring to each container enough memory for it to perform well) because it does not stop one container from starving the others of physical memory.
Furthermore, the metrics available to the kernel (\textit{e.g.} frequency of page faults, I/O requests, use of CPU cycle, \textit{etc.}) are not suitable for performance isolation, because they are not directly relevant to performance as experienced from the application perspective, which is better characterized by, for instance latency or throughput.

To avoid a container starving the others, the system administrator can limit the total amount of its physical memory.
If it would exceed its limit, some of its memory will be reclaimed, making it available to others.
In practice, the kernel tends to reclaim preferably from the file page cache; this causes a performance decrease for containers that access files \cite{the_kernel_development_community_concepts_nodate}.
The limits to container size are static, and do not adapt to the containers' dynamic behaviors.
This is problematic, because it is challenging to estimate the optimal amount of memory for an application to execute smoothly \cite{gregg_working_2018, nitu_working_2018}.

Our main contribution consists in an experimental demonstration of the limitations of the existing Linux mechanisms.

We organize the remainder of our paper as follows.
Section~\ref{sec_technical} presents some technical background.
Section~\ref{sec_problem} shows the issues with existing Linux mechanisms.
Finally, we conclude and discuss future work in Section~\ref{sec_conclusion}.